# Ethereum Test Network Setup - Geth

## Contents:
1. Introduction
2. File Rundown
3. Setting up the network


## Introduction
This is a basic list of steps that will set up a private blockchain network consisting of 3 nodes: 1 miner & 2 peers. 
This guide uses the go-ethereum (geth) implementation so the only prerequisite for it is having geth installed. This network can
be used to deploy and test smart contracts, test blockchain interactions and benchmark distributed applications. 


## File Rundown

I have included some files that will assist in getting this network set up. Most of them are shell scripts so you don't have to remember lengthy commands!

1. **start.sh**

	This script will open up [2/3] new terminal windows, 1 miner and 1/2 peers. It will also connect these nodes through enodes and start the mining. It has comments inside to change the configuration of how many nodes you want to start and connect.
	

1. **clear.sh**

	This script will create three directories (eth1|eth2|eth3) and initilise them with the genesis file genesis.json. This ensures that the nodes have seperate data directories as well as ensure that each node has the same genesis block and network id. If nodes start from different genesis blocks they will not be able to connect. 

	**You will have to change lines 9-11 to point to your installation of geth !!**

2. **genesis.json**

	This is a json object that contains certain parameters for the genesis block of a private blockchain. I will roughly explain each one:
	
	Config section contains special variables used by the inner workings of the blockchain. You can safely ignore most of these. The only important one is chainID. Nodes that wish to become peers MUST have the same chainID. 

	Alloc section contains pre-determined wallet address and alocates that wallet the balance given. (This is for use with pk-2 and pk-3)

	Difficulty is used to determine the inital difficulty for the miner. As the miner mines blocks this difficulty will converge to a much higer value. This speeds up the first 100 or so blocks mined.

	Gas Limit is set to the maxium value to ensure that transactions are not held up due to gas restrictions. 

	Everything else is set to 0's or a default value and can be ignored for a simple private test network! 

3. **pk-2/3**

	Each of these contain a json object that relates to the wallet address that are allocated ether in the genesis.json file. You do not need to touch these at all as they are placed in the correct location when ./clear.sh is called. Having pre allocated funds means that once the network is set up and mining all nodes will have access to ether. The passwords for these wallets are 'test'



## Setting up the network

Here I will explain the basic steps to setup the network

1. Move to a directory with three sub folders named eth1|eth2|eth3. 

2. Run ./clear.sh

3. Run ./start.sh




	###### Flag Descriptions inside scripts/start_node_x.sh

	**--datadir** specifies the dirctory the node will use to store chain data and its private keys.

	**--rpc & --rpcapi=** are used to open certain ports on the node so that it can be connected to via http connections using the web3 library. (Lets you access the node and blockchain in python/javascript)

	**--rpcport** If you are running multiple nodes on a local device then you need to specify different ports for each node you wish to connect to.

	**--networkid** is the id that node is looking for. This should be the same as in genesis.json.

	**--port** same as rpcport except for the node in general not just http connections

	**console** starts the node in javascript console mode. Gives you direct access to the node with a bunch of handy javascript tools. Look up geth javascript console api.

	**2&>/dev/null** is used to silence the output of the console. (I leave this option out on the third node as it can help with debugging to see the block information as it is mined.)



Thats it, you should now have a private blockchain network running and mining on your machine. From here you should be able to google anything geth related and apply it directly in the console of each node. 