#!/bin/sh
geth \
	--datadir eth1 \
	--rpc \
	--rpcapi="db,eth,net,web3,personal,admin,miner" \
	--rpcaddr 127.0.0.1 \
	--rpcport 8001 \
	--rpccorsdomain "*" \
	--networkid 881188 \
	--nat extip:127.0.0.1 \
	--nodiscover \
	--port 30301 \
	--verbosity "0" \
	--preload="helper.js" \
	console 