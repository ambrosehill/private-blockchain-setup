#!/bin/sh
geth \
	--datadir eth3 \
	--rpc \
	--rpcapi="db,eth,net,web3,personal,admin" \
	--rpcaddr 127.0.0.1 \
	--rpcport 8003 \
	--rpccorsdomain "*" \
	--networkid 881188 \
	--port 30303 \
	--nat extip:127.0.0.1 \
	--nodiscover \
	--preload="helper.js" \
	console