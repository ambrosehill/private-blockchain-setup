#!/bin/sh
geth \
 	--datadir eth2 \
 	--rpc \
 	--rpcapi="db,eth,net,web3,personal,admin" \
	--rpcaddr 127.0.0.1 \
	--rpcport 8002 \
	--rpccorsdomain "*" \
 	--networkid 881188 \
 	--port 30302 \
	--nat extip:127.0.0.1 \
	--nodiscover \
	--verbosity "0" \
	--preload="helper.js" \
 	console 
