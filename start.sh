#!/bin/sh
LOCALHOST=http://127.0.0.1
ADDR_1=$LOCALHOST:8001
ADDR_2=$LOCALHOST:8002
ADDR_3=$LOCALHOST:8003

#Run Clear.sh
./clear.sh

# Start all three nodes in the network
gnome-terminal --geometry 60x20+70+0 -- './scripts/start_node_1.sh'
# Uncomment the line below for setup with 3 nodes
#gnome-terminal --geometry 60x20+670+0 -- './scripts/start_node_2.sh'
gnome-terminal --geometry 60x20+1270+0 -- './scripts/start_node_3.sh'

# Sleep until all nodes have completely started
sleep 3

# Get enode information
enode1=`geth attach $ADDR_1 --exec 'admin.nodeInfo.enode'`
# Uncomment the line below for setup with 3 nodes
#enode2=`geth attach $ADDR_2 --exec 'admin.nodeInfo.enode'`
enode3=`geth attach $ADDR_3 --exec 'admin.nodeInfo.enode'`


# Connect all the nodes together as peers

: <<'END_COMMENT'
# Code for setting up a test network with 3 nodes
geth attach $ADDR_1 --exec "
	admin.addPeer(${enode2})
	admin.addPeer(${enode3})
"
geth attach $ADDR_2 --exec "
	admin.addPeer(${enode1})
	admin.addPeer(${enode3})
"
geth attach $ADDR_3 --exec "
	admin.addPeer(${enode1})
	admin.addPeer(${enode2})
"
# Remove the lines containing END_COMMENT to add a third node for testing purposes.
END_COMMENT

geth attach $ADDR_1 --exec "
	admin.addPeer(${enode3})
"

geth attach $ADDR_3 --exec "
	admin.addPeer(${enode1})
"

# Create a wallet in node 1
geth attach $ADDR_1 --exec "
	personal.newAccount('test')
	personal.unlockAccount(web3.eth.coinbase, 'test', 15000)
	miner.start()
"